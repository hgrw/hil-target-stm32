# Software Requirements
| Document Title          | Product System Requirements             |
| ----------------------- | --------------------------------------- |
| Document Owner          | Harry Roache-Wilson                     |
| Document Responsibility | Harry Roache-Wilson                     |
| Document Identifier     | 3                                       |
| Document Status         | draft                                   |
| Part of product         | product                                 |
| Part of release         | 1.0                                     |

## Scope of this document
This document specifies requirements and traceability for a demo application that uses the robust embedded systems engineering (RESE) methodology.

Traceability is achieved by an automated suite of hardware in the loop integration tests. Test results are dynamically updated in the traceability matrix.

## Conventions to be used

### Requirements guidelines
Each requirement has a unique identifier starting with the prefix “R”. Any words or phrases in all-caps refer to a formally defined word or phrase. Formal definitions are found in the [[Glossary]].

#### Requirements quality
#### Requirements identification
The key words "SHALL", "SHOULD", "MAY" in this document are to be interpreted as follows. Note that the requirement level of the document in which they are used modifies the force of these words. 
-   **SHALL**: This phrase, or the adjective "REQUIRED", means that the definition is an absolute requirement of the specification. 
-  **SHOULD**: This word, or the adjective "RECOMMENDED", means that there may exist valid reasons in particular circumstances to ignore a particular item, but the full implications must be understood and carefully weighed before choosing a different course. 
-   **MAY**: This word, or the adjective "OPTIONAL", means that an item is truly optional. 

### Acronyms and abbreviations
All technical terms used throughout this document – except the ones listed here – can be found in the [[Glossary]].

| Term | Description          |
| ---- | -------------------- |
| LED  | Light emitting diode |
| HIL  | Hardware in the loop | 

## Requirements Specification
### System Requirements
#### Functional Requirements
| RS001               | Product LEDs shall display a lighting animation that uses all four LEDs |
| ------------------- | ----------------------------------------------------------------------- |
| Status              | published                                                               |
| Description         |                                                                         |
| Rationale           |                                                                         |
| Dependencies        |                                                                         |
| Use-Case            |                                                                         |
| Supporting Material |                                                                         |

| RS002               | User shall be able to turn LEDs on and off with one button press |
| ------------------- | ---------------------------------------------------------------- |
| Status              | published                                                        |
| Description         |                                                                  |
| Rationale           |                                                                  |
| Dependencies        |                                                                  |
| Use-Case            |                                                                  |
| Supporting Material |                                                                  |

| RS003               | Product shall display a lighting pattern on startup without user input |
| ------------------- | ---------------------------------------------------------------------- |
| Status              | published                                                              |
| Description         |                                                                        |
| Rationale           |                                                                        |
| Dependencies        |                                                                        |
| Use-Case            |                                                                        |
| Supporting Material |                                                                        |

#### Non Functional Requirements
| RS004               | Product shall display a lighting pattern in less than 1 second after startup |
| ------------------- | ---------------------------------------------------------------------------- |
| Status              | published                                                                    |
| Description         |                                                                              |
| Rationale           |                                                                              |
| Dependencies        |                                                                              |
| Use-Case            |                                                                              |
| Supporting Material |                                                                              |

![[LED#LED Software Requirements]]

### 
*Without requirements and design, programming is the art of adding bugs to an empty text file.*
― Louis Srygley