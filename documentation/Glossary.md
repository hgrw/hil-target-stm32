| Document Title          | Product Glossary    |
| ----------------------- | ------------------- |
| Tags                    | #glossary           |
| Document Owner          | Harry Roache-Wilson |
| Document Responsibility | Harry Roache-Wilson |
| Document Identifier     | 2                   |
| Document Status         | draft               |
| Part of product         | product             |
| Part of release         | 1.0                 |