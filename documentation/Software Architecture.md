# Software Architecture
| Management              |                     |
| ----------------------- | ------------------- |
| Document Owner          | Harry Roache-Wilson |
| Document Responsibility | Harry Roache-Wilson |
| Document Status         | Published           | 
| Part of Release         | N/A                 |

## Introduction
### Purpose
### Scope
### Definitions, Acronyms and Abbreviations
Definitions, acronyms and abbreviations not defined here can be found in the [[Glossary]].

## System Context
The system developed here is to demonstrate a workflow that I think is interesting. As such, it is pretty simple. Target hardware is an STM32F4 discovery board, which has a few buttons, some LEDs, and exposes a range of IO pins, including the reset pin.

## Software Quality
Software quality is maintained and standardised using a range of static and dynamic analytics. Pre-commit hooks ensure atomicity and correctness of each commit. Tools are used to ensure quality of the application software, written in C++, and the test harness software, written in Python3.
- C++ tooling:
	- clang-tidy
	- clang-format
	- cppcheck
	- cpplint
	- cpputest
	- gcovr
	- GCC sanitisers (asan only)
- Python tooling:
	- black
	- isort
	- flake8
	- mypy

## Architectural Design
### Software Units
The software architecture satisfies [[Software Requirements]] and establishes a coherent set of software units that

1) are as small as possible without being trivial;
2) are arranged into a hierarchy of abstractions; and
3) abstractions have minimal horizontal dependencies.

Often a layer diagram is a nice way to depict the relationship between software units but in this case the system is so simple that units are described standalone.

| Software Unit | Description                                                                      |
| ------------- | -------------------------------------------------------------------------------- |
| core          | Main loop. Instantiates and executes application logic.                          |
| gpio          | GPIO driver for leds and buttons. Provides clean interface to get/set pin state. |
| leds          | Application logic for LED animations.                                            |
| button        | Button state getter including debouncing.                                        | 

### 
*Without requirements and design, programming is the art of adding bugs to an empty text file.*
― Louis Srygley