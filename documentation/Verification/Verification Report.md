# Verification Report
| Management              |                     |
| ----------------------- | ------------------- |
| Document Owner          | Harry Roache-Wilson |
| Document Responsibility | Harry Roache-Wilson |
| Document Status         | Draft               |
| Part of Release         | N/A                 | 
## Introduction

### Purpose

### Scope

### Definitions, Acronyms and Abbreviations
All technical terms used throughout this document – except the ones listed here – can be found in the [[Glossary]].

## Verification Methodology
Telemetry Controller verification is carried out at the system level and at the subsystem level. 

### System Verification
System requirements verification involves environmental testing and system-level integration testing. System verification is subject to limited automation

### Subsystem Verification
Subystem verification is concerned with software and hardware interfaces and is fully automated. Test simulation and mocks are incorporated into system verification as needed. Automated subsystem verification handled by ATS. Two kinds of tests are executed in CI pipelines:
1. Unit tests; and
2. Integration tests.

## Requirements Tracing

### 
*Without requirements and design, programming is the art of adding bugs to an empty text file.*
― Louis Srygley