## LED Software Design
### LED Software Requirements
#### Functional Requirements
| TCSR-[unit code]-[requirement code] | [requirement] | 
| ------------------- | ------------- |
| Status              | draft         |
| Description         |               |
| Rationale           |               |
| Dependencies        |               |
| Use-Case            |               |
| Supporting Material |               |

#### Non-Functional Requirements
| TCSR-[unit code]-[requirement code] | [requirement] | 
| ------------------- | ------------- |
| Status              | draft         |
| Description         |               |
| Rationale           |               |
| Dependencies        |               |
| Use-Case            |               |
| Supporting Material |               |

### LED Software Specifications
#### Hardware-Software Interface
#### Message Descriptions
### LED Software Architecture
#### Key Design Decisions
#### Class Diagram
#### State Transition Diagram
#### Sequence Diagram
#### Flow Chart
### LED Software Verification
#### Unit Test Traceability
#### Integration Test Traceability

### 
*Without requirements and design, programming is the art of adding bugs to an empty text file.*
― Louis Srygley