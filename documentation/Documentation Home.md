Documentation integrates software requirements specification, design documentation and test documentation with software implementation using a gitlab static site. Documentation tools are obsidian and doxygen. 

- [API Documentation](https://hgrw.gitlab.io/rese/doxygen/)
- [Developer Guidelines](https://hgrwilson.com/software-engineering/CI-CD%20for%20Embedded%20Systems,%20A%20Git%20Workflow.html)
- [[Software Requirements]]
- [[Software Architecture]]
- [[Integration Testing]]
- [[Verification Report]]
- [[Glossary]]

### 
*Without requirements and design, programming is the art of adding bugs to an empty text file.*
― Louis Srygley