## [unit] Software Design
### [unit] Software Requirements
#### Functional Requirements
| TCSR-[unit code]-[requirement code] | [requirement] | 
| ------------------- | ------------- |
| Status              | draft         |
| Description         |               |
| Rationale           |               |
| Dependencies        |               |
| Use-Case            |               |
| Supporting Material |               |

#### Non-Functional Requirements
| TCSR-[unit code]-[requirement code] | [requirement] | 
| ------------------- | ------------- |
| Status              | draft         |
| Description         |               |
| Rationale           |               |
| Dependencies        |               |
| Use-Case            |               |
| Supporting Material |               |

### [unit] Software Specifications
#### Hardware-Software Interface
#### Message Descriptions
### [unit] Software Architecture
#### Key Design Decisions
#### Class Diagram
#### State Transition Diagram
#### Sequence Diagram
#### Flow Chart
### [unit] Software Verification
#### Unit Test Traceability
#### Integration Test Traceability

### 
*Without requirements and design, programming is the art of adding bugs to an empty text file.*
― Louis Srygley