| Document Title          | Requirements |
| ----------------------- | ------------ |
| Tags                    |              |
| Document Owner          |              |
| Document Responsibility |              |
| Document Identifier     |              | 
| Document Status         |              |
| Part of product         |              |
| Part of release         |              |

# Scope of this document
This document specifies requirements and traceability for the _________ software project. The requirements provide a clear and concise specification against which software can be designed, implemented and tested.

Traceability is achieved through continuously integrated and deployed hardware-in-the-loop (HIL) software test harness. The requirements undergo automated validation testing  using the HIL tester and results are dynamically updated in the traceability matrix.
# Conventions to be used

## Requirements guideleines
Each requirement has a unique identifier starting with the prefix “R”. Any words or phrases in all-caps refer to a formally defined word or phrase. Formal definitions are found in the [[Glossary]].
### Requirements quality
### Requirements identification
The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT","SHOULD", "SHOULD NOT", "RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as follows. Note that the requirement level of the document in which they are used modifies the force of these words. 
-   MUST: This word, or the adjective "LEGALLY REQUIRED", means that the definition is an absolute requirement of the specification due to legal issues. 
-   MUST NOT: This phrase, or the phrase "MUST NOT", means that the definition is an absolute prohibition of the specification due to legal issues. 
-   SHALL: This phrase, or the adjective "REQUIRED", means that the definition is an absolute requirement of the specification. 
-   SHALL NOT: This phrase means that the definition is an absolute prohibition of the specification. 
-   SHOULD: This word, or the adjective "RECOMMENDED", means that there may exist valid reasons in particular circumstances to ignore a particular item, but the full implications must be understood and carefully weighed before choosing a different course. 
-   SHOULD NOT: This phrase, or the phrase "NOT RECOMMENDED", means that there may exist valid reasons in particular circumstances when the particular behavior is acceptable or even useful, but the full implications should be understood and the case carefully weighed before implementing any behavior described with this label. 
-   MAY: This word, or the adjective "OPTIONAL", means that an item is truly optional. One vendor may choose to include the item because a particular marketplace requires it or because the vendor feels that it enhances the product while another vendor may omit the same item. An implementation, which does not include a particular option, SHALL be prepared to interoperate with another implementation, which does include the option, though perhaps with reduced functionality. In the same vein an implementation, which does include a particular option, SHALL be prepared to interoperate with another implementation, which does not include the option (except, of course, for the feature the option provides.)
## Acronyms and abbreviations
All technical terms used throughout this document – except the ones listed here – can be found in the [[Glossary]].

| Term | Description |
| ---- | ----------- |
|      |             |
|      |             |

# Requirements Specification

| [insert id]         | [insert requirement] |
| ------------------- | -------------------- |
| Status              |                      | 
| Description         |                      |
| Rationale           |                      |
| Dependencies        |                      |
| Use-Case            |                      |
| Supporting Material |                      |

# Requirements Tracing
| Requirement | Description | Satisfied by |
| ----------- | ----------- | ------------ |
| [id]        |             |              | 

### 
*Without requirements and design, programming is the art of adding bugs to an empty text file.*
― Louis Srygley