#!/usr/bin/env bash

echo "collecting status for badges"

requirements_coverage=0.5
latest_release_tag=$(git describe --tags `git rev-list --tags --max-count=1`)
echo "{\"verification\":\"$requirements_coverage\", \"release_tag\":\"$latest_release_tag\"}" > verification_badges.json
