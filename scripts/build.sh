#!/usr/bin/env bash
BUILD_RELEASE=/tmp/pre-commit-build-release
BUILD_TEST=/tmp/pre-commit-build-utest
mkdir ${BUILD_RELEASE}
mkdir ${BUILD_TEST}

echo "Building firmware"
cmake -G Ninja -DCMAKE_TOOLCHAIN_FILE=cmake/arm-none-eabi-gcc.cmake -DCMAKE_BUILD_TYPE=Release -DENABLE_TESTING=OFF -DENABLE_PROFILING=OFF -B${BUILD_RELEASE} -H.
cmake --build ${BUILD_RELEASE} -- -j 6

echo "Building unit tests"
cmake -G Ninja -DCMAKE_TOOLCHAIN_FILE=cmake/host-utest-gcc.cmake -DENABLE_TESTING=ON -DENABLE_SANITIZERS=ON -DCMAKE_BUILD_TYPE=Release -B${BUILD_TEST} -H.
cmake --build ${BUILD_TEST} --target all -- -j 6

rm -rf ${BUILD_RELEASE}
rm -rf ${BUILD_TEST}
