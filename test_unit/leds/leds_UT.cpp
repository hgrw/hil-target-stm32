
/*
 * TEST(LEDS_001) : Test that flash animation toggles all LEDs twice
 * TEST(LEDS_002) : Test that spin animation toggles LEDs one at a time
 */

#include "CppUTest/TestHarness.h"
#include "leds/leds.hpp"
#include "mocks/gpio_mock.hpp"

CallBuffer callBuffer;  // NOLINT
StateTransitionBuffer stateTransitionBuffer;  // NOLINT

TEST_GROUP(gpio){void setup() override{// Init stuff
                                       callBuffer.delayCalled = false;
}

void teardown() override {
  // Un-init stuff
}
}
;

// Test that flash animation toggles all LEDs twice
TEST(gpio, LEDS_001) {
  leds::LedPainter leds;
  leds.animate(leds::AnimationStrategy::FLASH_ALL);
  CHECK_EQUAL_TEXT(true, callBuffer.delayCalled, "Delay never called");
}

// Test that spin animation toggles LEDs one at a time
TEST(gpio, LEDS_002) {
  leds::LedPainter leds;
  leds.animate(leds::AnimationStrategy::SPIN_SIMPLE);
  CHECK_EQUAL_TEXT(true, callBuffer.delayCalled, "Delay never called");

  // Check that pin
}
