//
// Created by mars on 11/23/22.
//

#ifndef RESE_STM32_GPIO_MOCK_H
#define RESE_STM32_GPIO_MOCK_H
#include <stm32f4xx_hal_gpio.h>

#include <array>

struct CallBuffer {
  bool toggleCalled{false};
  bool writeCalled{false};
  bool delayCalled{false};
};

enum class MockIndicators : uint16_t { DELAY, TOGGLE, HIGH, LOW };

constexpr auto STATE_TRANSITION_BUFFER_SIZE = 100;

using StateTransitionBuffer =
    std::array<MockIndicators, STATE_TRANSITION_BUFFER_SIZE>;

#endif  // RESE_STM32_GPIO_MOCK_H
