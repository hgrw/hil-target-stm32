//
// Created by mars on 11/23/22.
//

#include "gpio_mock.hpp"

#include <array>

// Expose GPIO mock to unit test
extern CallBuffer callBuffer;                        // NOLINT
extern StateTransitionBuffer stateTransitionBuffer;  // NOLINT

void HAL_GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin,
                       GPIO_PinState PinState) {
  callBuffer.writeCalled = true;
  stateTransitionBuffer.at(0) = (PinState == GPIO_PinState::GPIO_PIN_SET)
                                    ? MockIndicators::HIGH
                                    : MockIndicators::LOW;
}
void HAL_GPIO_TogglePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin) {
  callBuffer.toggleCalled = true;
  stateTransitionBuffer.at(0) = MockIndicators::TOGGLE;
}

#ifdef __cplusplus
extern "C" {
#endif

void HAL_Delay(uint32_t Delay) {
  callBuffer.delayCalled = true;
  stateTransitionBuffer.at(0) = MockIndicators::DELAY;
}

#ifdef __cplusplus
}
#endif
