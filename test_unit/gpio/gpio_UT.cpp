
/*
 * TEST(GPIO_001) : Test gpio toggle call
 * TEST(GPIO_002) : Test gpio pull high call
 * TEST(GPIO_003) : Test gpio pull low call
 */

#include <vector>

#include "CppUTest/TestHarness.h"
#include "gpio/gpio_driver.hpp"
#include "mocks/gpio_mock.hpp"

CallBuffer callBuffer;                        // NOLINT
StateTransitionBuffer stateTransitionBuffer;  // NOLINT

TEST_GROUP(gpio){void setup() override{// Init stuff
                                       callBuffer.toggleCalled = false;
callBuffer.writeCalled = false;
}

void teardown() override {
  // Un-init stuff
}
}
;

// Make sure toggle calls correct BSP function
TEST(gpio, GPIO_001) {
  const auto pinReg = gpio::PinRegisters();
  auto top = gpio::Pin(pinReg.ledTop);
  top.toggle();
  CHECK_EQUAL_TEXT(true, callBuffer.toggleCalled, "Pin toggle never called");
}

// Make sure pull high calls correct BSP function
TEST(gpio, GPIO_002) {
  const auto pinReg = gpio::PinRegisters();
  auto top = gpio::Pin(pinReg.ledTop);
  top.setHigh();
  MockIndicators command = stateTransitionBuffer.at(0);
  CHECK_EQUAL_TEXT(true, callBuffer.writeCalled, "Pin write never called");
  CHECK_TEXT(MockIndicators::HIGH == stateTransitionBuffer.at(0),
                   "Pin state set incorrectly");
}

// Make sure pull low calls correct BSP function
TEST(gpio, GPIO_003) {
  const auto pinReg = gpio::PinRegisters();
  auto top = gpio::Pin(pinReg.ledTop);
  top.setLow();
  CHECK_EQUAL_TEXT(true, callBuffer.writeCalled, "Pin write never called");
  CHECK_TEXT(MockIndicators::LOW == stateTransitionBuffer.at(0),
                   "Pin state set incorrectly");
}
