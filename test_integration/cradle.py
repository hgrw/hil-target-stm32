from devices.daq import LabJack
from devices.manager import DeviceManager


class Cradle:
    """
    Target software test harness. Holds all mocked devices.
    """

    def __init__(self):

        self.daq = LabJack()
        self.manager = DeviceManager()

        self.manager.register_device(self.daq)

    def cleanup(self):
        self.manager.stop()

    def hard_reset(self):
        """
        Power cycle the test harness to get it into a known state.
        """
        self.daq.reset_target()

    def transition_to_state(self, state):
        """
        Force test harness to have appropriate conditions to transition telemetry controller to
        input state.
        TODO - device state machine for Telemetry Controller
        TODO - complete ICD
        """
        pass
