import time
from enum import IntEnum

import u3

from .manager import Device


class LabJack(Device):
    SCAN_FREQUENCY = 100  # Hz
    MAX_REQUESTS = 500
    RESET_TIME_S = 0.1
    PIN_LOW, PIN_HIGH = range(2)
    PIN_INPUT, PIN_OUTPUT = range(2)

    class Pin(IntEnum):
        RESET = u3.EIO6
        USER_BUTTON = u3.EIO4
        TOP_LED = u3.EIO0
        RIGHT_LED = u3.EIO1
        BOTTOM_LED = u3.EIO2
        LEFT_LED = u3.EIO3

    def __init__(self):
        super().__init__()
        self._device = u3.U3()
        self._device.configIO(EIOAnalog=0)
        self._device.getCalibrationData()
        for pin in LabJack.Pin:
            if pin == LabJack.Pin.RESET or pin == LabJack.Pin.USER_BUTTON:
                u3.BitDirWrite(IONumber=pin.value, Direction=LabJack.PIN_OUTPUT)
            else:
                u3.BitDirWrite(IONumber=pin.value, Direction=LabJack.PIN_INPUT)
        self._device.streamConfig(
            NumChannels=1,
            PChannels=[193],
            NChannels=[193],
            Resolution=0,
            ScanFrequency=LabJack.SCAN_FREQUENCY,
        )
        self.errors = 0
        self.missed = 0

    def run(self):
        self._device.streamStart()
        while self._running:
            data = next(self._device.streamData(convert=True))
            print(data)
        self._device.streamStop()

    def cleanup(self):
        self.stop()

    def reset_target(self):
        self._device.setDOState(LabJack.Pin.RESET, LabJack.PIN_LOW)
        time.sleep(LabJack.RESET_TIME_S)
        self._device.setDOState(LabJack.Pin.RESET, LabJack.PIN_HIGH)
