from enum import IntEnum

import u3


class Button:
    class Voltage(IntEnum):
        ZERO = 0x0
        THREE = 0x99

    def __init__(self):
        self._pressed = u3.DAC0_8(Value=Button.Voltage.THREE)
        self._released = u3.DAC0_8(Value=Button.Voltage.ZERO)

    @property
    def press(self):
        return self._pressed

    @property
    def release(self):
        return self._released
