from threading import Thread


class DeviceManager:
    def __init__(self):
        self._devices = []

    def register_device(self, device):
        self._devices.append(device)

    def start(self):
        for device in self._devices:
            device.start()

    def stop(self):
        for device in self._devices:
            device.cleanup()

    def cleanup(self):
        self.stop()


class Device:
    def __init__(self):
        self._thread = None
        self._running = False

    def start(self):
        self._running = True
        self._thread = Thread(target=self.run)
        self._thread.start()

    def stop(self):
        self._running = False
        self._thread.join()

    def run(self):
        raise NotImplementedError

    def cleanup(self):
        raise NotImplementedError
