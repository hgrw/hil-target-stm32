import pytest

from ..cradle import Cradle


@pytest.fixture
def cradle():
    cradle = Cradle()
    cradle.manager.start()
    yield cradle
    cradle.cleanup()


@pytest.fixture
def reset(cradle):
    cradle.hard_reset()
