#!/usr/bin/env python3
# -*- coding: utf-8 -*-

if __name__ == "__main__":
    from devices.daq import LabJack
    from devices.manager import DeviceManager

    daq = LabJack()

    m = DeviceManager()
    m.register_device(daq)
    m.start()
    try:
        while True:
            pass
    except KeyboardInterrupt:
        m.stop()
        print("done")
