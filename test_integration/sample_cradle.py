#!/usr/bin/env python3
# -*- coding: utf-8 -*-

if __name__ == "__main__":
    from cradle import Cradle

    c = Cradle()
    c.manager.start()
    try:
        while True:
            pass
    except KeyboardInterrupt:
        c.cleanup()
        print("done")
