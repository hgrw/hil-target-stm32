# Hardware-in-the-loop Automated Test Harness - Target

For detailed information, [see documentation](https://hgrw.gitlab.io/hil-target-stm32/).

## Setup
Developrs are expected to have pre-commit and pre-push hooks installed:
```
# Install dependencies
pipenv install --dev

# Setup pre-commit and pre-push hooks
pipenv run pre-commit install -t pre-commit
pipenv run pre-commit install -t pre-push
```
