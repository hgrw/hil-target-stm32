# Include dependencies, application and examples
include(${BSP_DIR}/Filelists.cmake)
include(${APPLICATION_DIR}/Filelists.cmake)

# Generate compile command database for use with linter
if (${ENABLE_CLANG_TIDY})
    set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
    set(CMAKE_CXX_CLANG_TIDY
            clang-tidy;
            -checks=bugprone-*,cppcoreguidelines-*;
            -warnings-as-errors=*;)
endif ()

add_executable(${PROJECT_NAME}
        ${application_SRCS}
        ${application_HDRS}
        ${bsp_SRCS}
        ${bsp_HDRS}
        )

target_include_directories(${PROJECT_NAME} PRIVATE
        ${APPLICATION_DIR}
        )

target_include_directories(${PROJECT_NAME} SYSTEM PRIVATE
        ${BSP_DIR}
        ${BSP_DIR}/Inc
        ${BSP_DIR}/Drivers/STM32F4xx_HAL_Driver/Inc
        ${BSP_DIR}/Drivers/CMSIS/Device/ST/STM32F4xx/Include
        ${BSP_DIR}/Drivers/CMSIS/Include
        ${BSP_DIR}/Middlewares/ST/STM32_USB_Host_Library/Core/Inc
        ${BSP_DIR}/Middlewares/ST/STM32_USB_Host_Library/Class/CDC/Inc
        )

target_compile_options(${PROJECT_NAME} PRIVATE
        ${SANITIZER_COMPILER_FLAGS}
        ${PLATFORM_COMPILER_FLAGS}
        $<$<COMPILE_LANGUAGE:CXX>:@${CMAKE_DIR}/gcc-options-cxx.txt>
        $<$<COMPILE_LANGUAGE:CXX>:@${CMAKE_DIR}/gcc-warnings-cxx.txt>
        $<$<CONFIG:Debug>:-Og>
        )

target_link_options(${PROJECT_NAME} PRIVATE
        ${SANITIZER_LINKER_FLAGS}
        ${PLATFORM_COMPILER_FLAGS}
        -T ${BSP_DIR}/STM32F407VGTx_FLASH.ld
        -Wl,-Map=${PROJECT_NAME}.map,--cref
        -Wl,--gc-sections
        -Wl,--print-memory-usage
        )

target_compile_definitions(${PROJECT_NAME} PRIVATE
        -DUSE_HAL_DRIVER
        -DSTM32F407xx
        )

# Print executable size
add_custom_command(TARGET ${PROJECT_NAME}
        POST_BUILD
        COMMAND arm-none-eabi-size ${PROJECT_NAME})

# Create hex file
add_custom_command(TARGET ${PROJECT_NAME}
        POST_BUILD
        COMMAND arm-none-eabi-objcopy -O ihex ${PROJECT_NAME} ${PROJECT_NAME}.hex
        COMMAND arm-none-eabi-objcopy -O binary ${PROJECT_NAME} ${PROJECT_NAME}.bin)

# Improve clean target
set_target_properties(${PROJECT_NAME} PROPERTIES ADDITIONAL_CLEAN_FILES
        "${PROJECT_NAME}.bin;${PROJECT_NAME}.hex;${PROJECT_NAME}.map")

