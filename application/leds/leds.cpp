// Copyright (c) 2022. Harry Roache-Wilson. All Rights Reserved.

#include "leds/leds.hpp"

using leds::LedPainter;
using leds::PinArray;

LedPainter::LedPainter()
    : pinRegisters(),
      pins{{gpio::Pin(pinRegisters.ledTop), gpio::Pin(pinRegisters.ledRight),
            gpio::Pin(pinRegisters.ledBottom),
            gpio::Pin(pinRegisters.ledLeft)}} {}
auto LedPainter::animate(leds::AnimationStrategy strat) -> void {
  switch (strat) {
    case (leds::AnimationStrategy::FLASH_ALL):
      for (Pin& pin : pins) {
        pin.toggle();
      }
      HAL_Delay(leds::flashPeriodMs);
      for (Pin& pin : pins) {
        pin.toggle();
      }
      HAL_Delay(leds::flashPeriodMs);
      break;
    case (leds::AnimationStrategy::SPIN_SIMPLE):
      for (Pin& pin : pins) {
        pin.toggle();
        HAL_Delay(leds::spinPeriodMs);
      }
      for (Pin& pin : pins) {
        pin.toggle();
        HAL_Delay(leds::spinPeriodMs);
      }
      break;
    default:
      break;
  }
}
