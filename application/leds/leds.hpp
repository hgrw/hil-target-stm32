// Copyright (c) 2022. Harry Roache-Wilson. All Rights Reserved.

#ifndef APPLICATION_LEDS_LEDS_HPP_
#define APPLICATION_LEDS_LEDS_HPP_

#include <array>

#include "gpio/gpio_driver.hpp"

namespace leds {
using gpio::Pin;
constexpr auto numLeds = 4;
constexpr auto flashFrequencyHz = 2;
constexpr auto spinFrequencyHz = 8;
constexpr auto flashPeriodMs{
    static_cast<uint32_t>((1.0 / flashFrequencyHz) * 1000U)};
constexpr auto spinPeriodMs{
    static_cast<uint32_t>((1.0 / spinFrequencyHz) * 1000U)};
using PinArray = std::array<Pin, numLeds>;

enum class AnimationStrategy : uint8_t { FLASH_ALL, SPIN_SIMPLE, SPIN_COMPLEX };

class LedPainter {
 public:
  LedPainter();
  auto animate(AnimationStrategy strat) -> void;

 private:
  gpio::PinRegisters pinRegisters;
  PinArray pins;
};
}  // namespace leds

#endif  // APPLICATION_LEDS_LEDS_HPP_
