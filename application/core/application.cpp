// Copyright (c) 2022. Harry Roache-Wilson. All Rights Reserved.

#include "core/application.h"

#include "leds/leds.hpp"

void appMain() {
  while (true) {  // NOLINT
    leds::LedPainter leds;
    leds.animate(leds::AnimationStrategy::FLASH_ALL);
    leds.animate(leds::AnimationStrategy::FLASH_ALL);
    leds.animate(leds::AnimationStrategy::SPIN_SIMPLE);
  }
}
