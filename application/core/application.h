// Copyright (c) 2022. Harry Roache-Wilson. All Rights Reserved.

#ifndef APPLICATION_CORE_APPLICATION_H_
#define APPLICATION_CORE_APPLICATION_H_

#ifdef __cplusplus
extern "C" {
#endif

void appMain();

#ifdef __cplusplus
}
#endif

#endif  // APPLICATION_CORE_APPLICATION_H_
