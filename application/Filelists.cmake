# Application files
set(application_SRCS
        ${APPLICATION_DIR}/core/application.cpp
        ${APPLICATION_DIR}/gpio/gpio_driver.cpp
        ${APPLICATION_DIR}/leds/leds.cpp
        )

set(application_HDRS
        ${APPLICATION_DIR}/core/application.h
        ${APPLICATION_DIR}/gpio/gpio_driver.hpp
        ${APPLICATION_DIR}/leds/leds.hpp
        )
