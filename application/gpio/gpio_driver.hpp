// Copyright (c) 2022. Harry Roache-Wilson. All Rights Reserved.

#ifndef APPLICATION_GPIO_GPIO_DRIVER_HPP_
#define APPLICATION_GPIO_GPIO_DRIVER_HPP_

#include <array>
#include <cstdint>
#include <utility>

#include "Drivers/CMSIS/Device/ST/STM32F4xx/Include/stm32f407xx.h"
#include "Inc/main.h"

namespace gpio {

struct PinRegister {
  GPIO_TypeDef* port;
  uint16_t pin;
};

// Each GPIO pin has a port and a pin
struct PinRegisters {
  PinRegister ledTop{LD3_GPIO_Port, LD3_Pin};
  PinRegister ledLeft{LD5_GPIO_Port, LD4_Pin};
  PinRegister ledBottom{LD4_GPIO_Port, LD6_Pin};
  PinRegister ledRight{LD6_GPIO_Port, LD5_Pin};
};

class Pin {
 public:
  explicit Pin(PinRegister pinReg);
  auto toggle() -> void;
  auto setHigh() -> void;
  auto setLow() -> void;

 private:
  PinRegister pinRegister;
};

}  // namespace gpio

#endif  // APPLICATION_GPIO_GPIO_DRIVER_HPP_
