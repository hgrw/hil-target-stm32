// Copyright (c) 2022. Harry Roache-Wilson. All Rights Reserved.

#include "gpio/gpio_driver.hpp"

using gpio::Pin;
using gpio::PinRegister;
using std::get;

Pin::Pin(PinRegister pinReg) : pinRegister(pinReg) {}

void Pin::toggle() { HAL_GPIO_TogglePin(pinRegister.port, pinRegister.pin); }
void Pin::setHigh() {
  HAL_GPIO_WritePin(pinRegister.port, pinRegister.pin,
                    GPIO_PinState::GPIO_PIN_SET);
}
void Pin::setLow() {
  HAL_GPIO_WritePin(pinRegister.port, pinRegister.pin,
                    GPIO_PinState::GPIO_PIN_RESET);
}
